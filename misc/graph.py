#!/usr/bin/env python3
from itertools import count

from pygraphviz import AGraph

_counter = count(1)
c = lambda: next(_counter)

graph = AGraph(name='graph', directed=True, strict=False)

graph.add_node('pod1_app', label='Application 1', sortv=1)
graph.add_node('pod2_app', label='Application 2', sortv=2)

graph.add_node('node_docker', label='Docker')
graph.add_node('node_custodia', label='Custodia')
graph.add_node('node_proc', label='/proc/PID/cgroup')

graph.add_node('master_custodia', label='Custodia')
graph.add_node('master_apiserver', label='kube apiserver')
graph.add_node('master_etcd', label='etcd')

graph.add_node('customer_custodia', label='Custodia')
graph.add_node('customer_sqlite', label='sqlite')

graph.add_edge('pod1_app', 'node_custodia', label='%i) Ask for secret\n[unix]' % c(), style='bold')
graph.add_edge('pod2_app', 'node_custodia', label='Ask for secret\n[unix]')

graph.add_edge('node_custodia', 'node_proc', label='%i) Translate PID\nto dockerid' % c(), dir='both')
graph.add_edge('node_custodia', 'node_docker', label='%i) Get k8s namespace/pod\nby docker id' % c(), dir='both')
graph.add_edge('node_custodia', 'master_custodia', label='%i) Ask for secret\n[tcp]' % c(), style='bold')

graph.add_edge('master_custodia', 'master_etcd', label='%i) Auth' % c())
graph.add_edge('master_custodia', 'master_apiserver', label='%i) Get secret namespace\nby namespace/pod' % c(), dir='both')
# graph.add_edge('master_apiserver', 'node_docker', dir='both')
graph.add_edge('master_custodia', 'customer_custodia', label='%i) Ask for secret\n[tcp]' % c(), style='bold')

graph.add_edge('customer_custodia', 'customer_sqlite', 'customer_sqlite_auth', label='%i) Auth' % c())
graph.add_edge('customer_custodia', 'customer_sqlite', 'customer_sqlite_secret', label='%i) Fetch secret' % c(), style='bold')

graph.add_edge('customer_custodia', 'master_custodia', 'customer_sqlite_secret', label='%i) Return secret' % c(), style='dashed')
graph.add_edge('master_custodia', 'node_custodia', 'customer_sqlite_secret', label='%i) Return secret' % c(), style='dashed')
graph.add_edge('node_custodia', 'pod1_app', 'customer_sqlite_secret', label='%i) Return secret' % c(), style='dashed')

SUBGRAPHS = [
    ['node', None, dict(label="Kubernetes Node")],
    ['pod2', 'node', dict(label="Pod 2")],
    ['pod1', 'node', dict(label="Pod 1")],
    ['master', None, dict(label="Kubernetes Master")],
    ['customer', None, dict(label="Customer")],
]

for name, parent, kwargs in SUBGRAPHS:
    if parent is None:
        parent = graph
    else:
        parent = graph.get_subgraph("cluster_%s" % parent)
    sub = parent.add_subgraph(name="cluster_%s" % name, **kwargs)
    sub.add_nodes_from(n for n in graph.nodes() if n.startswith(name))


if __name__ == '__main__':
    graph.layout('dot')
    print(graph.string())
    graph.draw('graph.svg', format='svg')
