#!/bin/sh
set -e

REPOS=https://fedorapeople.org/cgit/simo/public_git/custodia.git

if [ ! -d custodia ]; then
    git clone $REPOS
fi

pushd custodia > /dev/null
git fetch --all
git pull --rebase
popd > /dev/null

sudo docker build -t 10.34.78.249:5000/custodia-server .
sudo docker push 10.34.78.249:5000/custodia-server

