FROM fedora
MAINTAINER Christian Heimes

RUN dnf -y update && dnf clean all
RUN dnf -y install dnf-plugins-core python python-flask python-requests && \
    dnf clean all
RUN dnf -y copr enable simo/jwcrypto && \
    dnf -y install python-jwcrypto python-cryptography && \
    dnf clean all

VOLUME ["/custodia-data"]
EXPOSE 8080

ADD . /custodia-server

CMD ["/custodia-server/run.sh"]
